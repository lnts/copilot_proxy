# GitHub Copilot 代理工具

此工具作为 GitHub Copilot 请求的代理层，确保您的 IP 地址保持私密并管理遥测数据以减少数据上传。它还有助于避免被 GitHub 错误地标记为风险用户。以下是针对 Visual Studio Code 和 IntelliJ IDEA 插件的设置指南。

## 特点

- **隐私保护**：防止您的 IP 地址泄露给 GitHub。
- **减少数据遥测**：最小化发送到 GitHub 的数据量。
- **避免账户被标记**：帮助防止您的账户被错误地标记为风险。

## 设置指南

### 对于 Visual Studio Code

1. **安装 GitHub Copilot 插件**（如果尚未安装）。
2. **修改 VSCode 设置**，通过在您的 `settings.json` 文件中添加以下配置来使用此代理：

    ```json5
    {
        "github.copilot.advanced": {
            "authProvider": "github-enterprise",
            // 设置代理 copilot 提示请求时
            "debug.overrideProxyUrl": "https://api.your.domain",
            // 设置代理 copilot 聊天提示请求时
            "debug.chatOverrideProxyUrl": "https://api.your.domain/chat/completions",
            // 使用 GPT-4 模型进行 copilot-chat，无需代理服务器即可使用
            "debug.overrideChatEngine": "gpt-4",
        },
        "github-enterprise.uri": "https://your.domain",
    }
    ```

### 对于 IntelliJ IDEA

1. **设置环境变量**：在您的系统上配置以下环境变量：

    ```plaintext
    GH_COPILOT_OVERRIDE_PROXY_URL=https://api.your.domain
    GH_COPILOT_OVERRIDE_CAPI_URL=https://api.your.domain
    ```

2. **配置 IntelliJ IDEA 中的 GitHub Copilot 插件**：
    - 转到 `设置` > `语言与框架` > `GitHub Copilot` > `认证`。
    - 将 `认证提供者` 设置为 `your.domain`。

确保所有指向 api.your.domain 和 your.domain 的请求都通过此代理程序进行路由，以确保功能正常并增强安全措施。

## 配置

默认情况下，工具从当前运行目录读取配置文件。您也可以在启动工具时使用 `--config` 或 `-c` 选项指定配置文件。确保配置文件遵循本文档前面提到的 JSON 结构。

### 配置文件格式

以下是工具的示例配置：

```json
{
  "listenIp": "0.0.0.0",
  "listenPort": 8080,
  "httpProxyAddrList": [
    "127.0.0.1:7890",
    "127.0.0.1:7891"
  ],
  "githubTokenList": [
    "gho_Fr0Xcd07iishNhaJuxOvvkwa6dzHKg2nrJeQ"
  ],
  "tokenSalt": "default_salt",
  "adminToken": "default_admin_token"
}
```

- **listenIp** 和 **listenPort**：指定代理服务器监听传入请求的 IP 地址和端口。
- **httpProxyAddrList**：工具用于转发请求的 HTTP 代理地址列表。
- **githubTokenList**：用于验证 GitHub 请求的 GitHub 令牌列表。
- **tokenSalt**：用于令牌加密的盐值字符串。
- **adminToken**：用于管理操作的令牌。

### 持久性

在退出时，工具会在配置文件所在目录保存几个 JSON 文件，以便数据持久化：

- `access_token.json`：存储访问令牌。
- `client_ip.json`：存储客户端 IP 地址。
- `header.json`：存储头信息。
- `token.json`：存储加密令牌。
- `user.json`：存储用户信息

。

### 日志

默认启用日志记录，在运行路径生成 `copilot_proxy.log` 文件。如果偏好，可使用 `--no-log` 选项禁用日志记录。

## 使用

要使用特定配置启动代理服务器，请使用以下命令：

```bash
copilot_proxy --config /path/to/your_config.json
```

将 `/path/to/your_config.json` 替换为您的配置文件的实际路径。此命令使用您在配置文件中定义的设置初始化代理服务器。

## API 路由

GitHub Copilot 代理工具设置了多个路由以处理认证、用户数据请求和遥测等功能。以下是可用路由及其功能的细分：

### 认证路由

- **POST `/login/device/code`**：启动设备代码登录流程。
- **GET `/login/device`**：检索设备代码登录尝试的状态。
- **POST `/login/oauth/access_token`**：将设备代码交换为 OAuth 访问令牌。需要设备代码授权。

### 用户数据路由

- **GET `/api/v3/user` 和 GET `/user`**：获取用户详情。需要访问令牌授权。
- **GET `/api/v3/meta`**：检索与 GitHub API 服务相关的元数据。需要访问令牌授权。
- **GET `/copilot_internal/v2/token`**：获取 Copilot 服务内部使用的令牌。需要访问令牌授权。

### Copilot 请求代理

- **POST `/v1/engines/copilot-codex/completions`**：代理完成请求到官方 Copilot Codex 端点。需要 Copilot 令牌授权。
- **POST `/chat/completions`**：代理聊天完成请求到官方 GitHub Copilot 聊天 API。需要 Copilot 令牌授权。

### 遥测

- **POST `/telemetry`**：处理遥测数据的发布，不处理正文。

### 管理路由

- **POST `/header/upload_token`**：允许管理员上传令牌以更新请求使用的头信息。需要管理员令牌授权。
- **POST `/github/upload_token`**：允许上传 GitHub 用户令牌以用于代理请求。需要管理员令牌授权。
- **POST `/json/save`**：以 JSON 格式保存配置或状态信息。需要管理员令牌授权。

## 支持

如有问题、疑问或希望贡献，请参考 [GitLab 上的项目仓库](https://gitlab.com/LaelLuo/copilot_proxy) 或在那里提出问题。Linux AMD64 版本的构建可作为 CI 工件下载。

## 许可

[MIT 许可证](LICENSE) - 您可以根据 MIT 许可证条款自由使用、修改和分发。