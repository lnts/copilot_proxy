import 'package:copilot_proxy/common.dart';
import 'package:copilot_proxy/context.dart';
import 'package:copilot_proxy/middleware/middleware.dart';

final Map<String, String> _userDeviceCodes = {};

final Map<String, String> _clientDeviceCodes = {};

(String, String) generateUserCode(String clientId) {
  final deviceCode = uuid.v4();
  String gen() {
    return List.generate(6, (index) {
      return chars[random.nextInt(chars.length)];
    }).join();
  }

  var userCode = gen();
  while (_userDeviceCodes.containsKey(userCode)) {
    userCode = gen();
  }

  _clientDeviceCodes[clientId] = deviceCode;
  _userDeviceCodes[userCode] = deviceCode;
  return (userCode, deviceCode);
}

final List<String> _deviceCodes = [];

bool checkUserCode(String? userCode) {
  return _userDeviceCodes.containsKey(userCode);
}

bool checkClientID(String? clientId) {
  return _clientDeviceCodes.containsKey(clientId);
}

bool checkDeviceCode(String? deviceCode) {
  return _deviceCodes.contains(deviceCode);
}

void allowDeviceCode(String userCode, [String? clientId]) {
  final deviceCode = _userDeviceCodes.remove(userCode);
  if (clientId != null) _clientDeviceCodes.remove(clientId);
  if (deviceCode == null) return;
  _deviceCodes.add(deviceCode);
}

void removeDeviceCode(String deviceCode) {
  _deviceCodes.remove(deviceCode);
}

Future<void> deviceCodeCheckAuth(
  String? _,
  Context context,
  Next next,
) async {
  if (!checkDeviceCode(context['device_code'])) {
    context.ok();
    context.json({
      'error': 'authorization_pending',
      'error_description': 'The authorization request is still pending.',
      'error_uri': 'https://docs.github.com/developers/apps/authorizing-oauth-apps#error-codes-for-the-device-flow',
    });
    return;
  }
  context.auth = true;
  await next();
}
