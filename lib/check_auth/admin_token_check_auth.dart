import 'package:copilot_proxy/common.dart';
import 'package:copilot_proxy/context.dart';
import 'package:copilot_proxy/middleware/middleware.dart';

Future<void> adminTokenCheckAuth(
  String? token,
  Context context,
  Next next,
) async {
  if (config.adminToken != token) return;
  context.auth = true;
  await next();
}
