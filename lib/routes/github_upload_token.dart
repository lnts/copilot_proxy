import 'package:copilot_proxy/context.dart';
import 'package:copilot_proxy/token_manager.dart';

Future<void> postGithubUploadToken(Context context) async {
  final githubToken = context['githubToken'];
  TokenManager.instance.addGithubToken(githubToken);
  context.noContent();
}
